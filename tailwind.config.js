module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    // "./nuxt.config.{js,ts}",
  ],
  theme: {
    fontFamily: {
      'lato': ['lato', 'sans-serif']
    },
    extend: {
      colors: {
        // transparent: 'transparent',
        // current: 'currentColor',
        'test-green': '#319795',
        'test-light-green': '#E6FFFA',
        'test-blue': '#3182CE',
        'test-black': '#2D3748',
        'test-tab-green': '#81E6D9',
        'test-big-text': '#718096',
        'test-small-text': '#4A5568',
        'test-border-gray': '#CBD5E0',
      },
      borderRadius: {
        'largex': '12px',
      },
      boxShadow: {
        'navbar': '0px 3px 6px #00000029',
      },
    },
  },
  plugins: [],
}
